package micro;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import utility.Utility;

public class UtilityTest {
	
	private final static Logger log = Logger.getLogger(UtilityTest.class);
	private Utility utility;
	private int number;
	private String cadena;
	
	@Before
	public void before() {
		log.info("inicia bateria de pruebas");
		cadena = null;
		utility = null;
	}
	
	@Test
	public void intToStringTest() {
		utility = new Utility();
		number=56;
		cadena=utility.intToString(number);
		log.info("numero original "+number);
		log.info("Cadena numero:"+cadena);
		assertEquals(String.valueOf(number), cadena);
		
	}
	
	@After
	public void After() {
		cadena = null;
		utility = null;
		log.info("termina bateria de pruebas");
	}

}
