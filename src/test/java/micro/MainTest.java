package micro;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MainTest {
	
	private final static Logger log = Logger.getLogger(MainTest.class);
	private static MainApplication main;
	private String[] args;
	
	@Before
	public void Before() {
		main=null;
		args=null;
	}
	
	@Test
	public void mainTest() {
		main = new MainApplication();
		
		args = new String[]{""};
		MainApplication.main(args);
		assert(true);
	}
	
	
	@After
	public void After() {
		main=null;
		args=null;
		
	}
	
	
	

}
